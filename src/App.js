import React, { useState } from 'react';
import { Line } from 'react-chartjs-2';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
} from 'chart.js';
import {
  FormControl,
  Select,
  MenuItem,
  Checkbox,
  ListItemText,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  TablePagination
} from '@mui/material';
import {weatherDataJson} from './data/weatherData';
import Footer from './components/Footer';
import Header from './components/Header';
ChartJS.register(CategoryScale, LinearScale, PointElement, LineElement, Title, Tooltip, Legend);

const weatherIcons = {
  'clear sky': '☀️',
  'few clouds': '🌤',
  'mist': '🌫',
  'rain': '🌧',
  'shower rain': '🌦',
  'broken clouds': '☁️',
  'thunderstorm': '⛈',
  'snow': '❄️',
  'scattered clouds': '🌥'
};

const getBackgroundColor = (col, value) => {
  switch (col) {
    case 'Temperature':
      return value > 25 ? 'bg-red-100' : value > 15 ? 'bg-yellow-100' : 'bg-blue-100';
    case 'Humidity':
      return value > 75 ? 'bg-blue-200' : value > 50 ? 'bg-green-200' : 'bg-yellow-200';
    case 'Wind Speed':
      return value > 8 ? 'bg-gray-200' : value > 4 ? 'bg-green-200' : 'bg-yellow-200';
    default:
      return '';
  }
};

const WeatherTable = ({ data, columns }) => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <Paper>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="weather data table">
          <TableHead>
            <TableRow>
              {columns.map((col) => (
                <TableCell key={col}>{col}</TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row, index) => (
              <TableRow key={index}>
                {columns.map((col) => (
                  <TableCell key={col} className={getBackgroundColor(col, row[col])}>
                    {col === 'Weather Description' ? (
                      <span>{weatherIcons[row[col]]} {row[col]}</span>
                    ) : (
                      row[col]
                    )}
                  </TableCell>
                ))}
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[5, 10, 15]}
        component="div"
        count={data.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Paper>
  );
};

const WeatherChart = ({ columns }) => {
  const [selectedColumns, setSelectedColumns] = useState(['Temperature', 'Humidity', 'Wind Speed']);

  const handleChange = (event) => {
    const {
      target: { value }
    } = event;
    setSelectedColumns(
      typeof value === 'string' ? value.split(',') : value
    );
  };

  const datasets = selectedColumns.map((col) => ({
    label: col,
    data: weatherDataJson.map((d) => d[col]),
    borderColor:
      col === 'Temperature'
        ? 'rgb(75, 192, 192)'
        : col === 'Humidity'
        ? 'rgb(153, 102, 255)'
        : 'rgb(255, 159, 64)',
    backgroundColor:
      col === 'Temperature'
        ? 'rgba(75, 192, 192, 0.2)'
        : col === 'Humidity'
        ? 'rgba(153, 102, 255, 0.2)'
        : 'rgba(255, 159, 64, 0.2)',
    yAxisID: col === 'Temperature' ? 'y1' : col === 'Humidity' ? 'y2' : 'y3',
    pointStyle: 'circle',
  }));

  const data = {
    labels: weatherDataJson.map((d) => d.Datetime),
    datasets
  };

  const options = {
    responsive: true,
    interaction: {
      mode: 'index',
      intersect: false
    },
    stacked: false,
    plugins: {
      title: {
        display: true,
        text: 'Weather Data'
      },
      legend: {
        labels: {
          usePointStyle: true
        }
      }
    },
    scales: {
      y1: {
        type: 'linear',
        display: true,
        position: 'left',
        title: {
          display: true,
          text: 'Temperature (°C)'
        }
      },
      y2: {
        type: 'linear',
        display: true,
        position: 'right',
        grid: {
          drawOnChartArea: false
        },
        title: {
          display: true,
          text: 'Humidity (%)'
        }
      },
      y3: {
        type: 'linear',
        display: true,
        position: 'right',
        grid: {
          drawOnChartArea: false
        },
        title: {
          display: true,
          text: 'Wind Speed (m/s)'
        }
      }
    }
  };

  return (
    <div className="w-full">
      <FormControl sx={{ m: 1, width: 300 }}>
        <label className='mt-3 text-xl '>Filter Charts</label>
        <Select
          multiple
          value={selectedColumns}
          onChange={handleChange}
          renderValue={(selected) => selected.join(', ')}
        >
          {columns.map((col) => (
            <MenuItem key={col} value={col}>
              <Checkbox checked={selectedColumns.indexOf(col) > -1} />
              <ListItemText primary={col} />
            </MenuItem>
          ))}
        </Select>
      </FormControl>
      <Line data={data} options={options} />
    </div>
  );
};

const WeatherDashboard = () => {
  const columns = ['Temperature', 'Humidity', 'Wind Speed'];
  const weatherDataGrouped = weatherDataJson.map((data) => ({
    ...data,
    City: 'Angers'
  }));

  return (
   <>
   <Header/>
    <div className="flex flex-col p-4 w-full">
      <div className="w-full mb-8">
        <WeatherChart columns={columns} />
      </div>
      <div className="w-full mt-8">
        <WeatherTable data={weatherDataGrouped} columns={columns} />
      </div>
    </div>
    <Footer/>
   </>
  );
};

export default WeatherDashboard;
