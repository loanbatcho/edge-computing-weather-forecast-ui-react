import React from "react";

function Header() {
  return (
    <>
      <header className="flex justify-between">
        <span className="flex items-center px-5 mr-2 font-bold ">
          <img
            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRckFyHQNwQBvRuv0zR9hu36i_Dymk4S-G_b5Ur6M11Kw&s"
            className="h-6 sm:h-9"
            alt="Weather Logo"
          />
          <span className="self-center text-2xl whitespace-nowrap dark:text-white">
            Weather Dashboard
          </span>
        </span>
        <span className="flex items-center px-5 mr-2 font-bold ">
          <img
            src="https://cdn-icons-png.flaticon.com/512/10435/10435236.png"
            className="h-6 sm:h-9"
            alt="Weather Logo"
          />
         
        </span>
      </header>
    </>
  );
}

export default Header;
